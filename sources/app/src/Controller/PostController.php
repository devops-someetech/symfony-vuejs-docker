<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

//use Psr\Log\LoggerInterface;
//use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @Rest\Route("/api")
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 */
final class PostController extends AbstractController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var SerializerInterface */
    private $serializer;

    public function __construct(
        EntityManagerInterface $em,
        SerializerInterface $serializer
    ) {
                                //,
                                //LoggerInterface $logger,
                                //AuthenticationUtils $auth)
        $this->em = $em;
        $this->serializer = $serializer;
        // $this->logger = $logger;
        // $this->auth = $auth;
    }

    /**
     * @throws BadRequestHttpException
     *
     * @Rest\Post("/posts", name="createPost")
     * @IsGranted("ROLE_FOO")
     */
    public function createAction(Request $request): JsonResponse
    {
        // On récupère la dernière erreur d'authentification s'il yen a
        //$error = $this->auth->getLastAuthenticationError();
        //On récupère le dernier identifiant utilisé pour une connexion
        //$username = $this->auth->getLastUsername();
        //$this->logger->alert($username); //('test user credentials : '+ $error + ' + ' $username);

        // VueJS n'arrive pas à envoyer correctement ou SF a récupérer
        // $message = $request->request->get('message');

        $data = $request->getContent();
        $content = json_decode($data);
        $message = strval($content->message);

        if (empty($message)) {
            //throw new BadRequestHttpException('message cannot be empty :' . $message);
            throw new BadRequestHttpException('message cannot be empty');
        }

        $post = new Post();
        $post->setMessage($message);
        $this->em->persist($post);
        $this->em->flush();
        $data = $this->serializer->serialize($post, JsonEncoder::FORMAT);

        return new JsonResponse($data, Response::HTTP_CREATED, [], true);
    }

    /**
     * @Rest\Get("/posts", name="findAllPosts")
     */
    public function findAllAction(): JsonResponse
    {
        $posts = $this->em->getRepository(Post::class)->findBy([], ['id' => 'DESC']);
        $data = $this->serializer->serialize($posts, JsonEncoder::FORMAT);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }
}
